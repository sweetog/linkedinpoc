import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LinkedInProfileService {
    apiURL: string = 'https://localhost:44319/api/linkedinprofile';

    constructor(private http: HttpClient) { }

    public getProfile(code: string) {

        //return this.http.get(`${this.apiURL}/${code}`, { responseType: 'text' });

        return this.http.get(`${this.apiURL}/${code}`);

    }

    public test() {

        return this.http.get(this.apiURL);

    }
}