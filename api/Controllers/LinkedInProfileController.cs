﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinkedInPoc.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace LinkedInPoc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinkedInProfileController : ControllerBase
    {
        private readonly ILinkedInService _linkedInService;

        public LinkedInProfileController(ILinkedInService linkedInService)
        {
            _linkedInService = linkedInService;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var token = await _linkedInService.GetAccessToken(code);
            var profile = await _linkedInService.GetProfile(token);
            var email = await _linkedInService.GetEmail(token);
            profile["email"] = email["elements"][0]["handle~"]["emailAddress"];
            return Ok(profile);
        }

        // GET api/linkedinprofile
        //[HttpGet]
        //public ActionResult<IEnumerable<string>> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
