﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkedInPoc.Models
{
    public class AppSettings
    {
        public string LinkedInAuthUrl { get; set; }
        /// <summary>
        /// The Linked App ClientId Requesting Authorization to Access LinkedIn Member Profiles
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// The Callback Uri/Url that LinkedIn will call after member responds to authorization permissions
        /// </summary>
        public string RedirectUri { get; set; }

        public string LinkedInAccessTokenUrl { get; set; }

        public string LinkedInProfileUrl { get; set; }

        public string LinkedInEmailUrl { get; set; }

        /// <summary>
        /// NOT SECURE POC only
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
