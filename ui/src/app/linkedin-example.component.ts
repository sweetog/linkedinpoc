import { Component, OnInit } from '@angular/core';
import { LinkedInProfileService } from './@core/linkedin-profile.service';

@Component({
    selector: 'linkedin-example',
    template: '<a [href]="url">Click Here to Test LinkedIn Profile/OAuth</a>'
})

export class LinkedInExample implements OnInit {
    client_id: string = '86wa7a0dlpzlxz';
    scope: string = 'r_liteprofile r_emailaddress'; //r_basicprofile, r_fullprofile
	redirect_uri:string = encodeURI("http://localhost:4200/signin-linkedin");
    url: string = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${this.client_id}&redirect_uri=${this.redirect_uri}&scope=${this.scope}`;

    constructor(private s: LinkedInProfileService){}
    
    ngOnInit() {
        // this.s.test()
        // .subscribe((resp: String) => {
        //     console.log('hello test complete');
        //     console.log('resp', resp);
        // });
    }

}