﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LinkedInPoc.Interfaces
{
    public interface ILinkedInRouter
    {
        string AuthorizationCodeRequestLightProfileUrlEncoded(string state);
        string ProfileRequestUrl();
        string EmailRequestUrl();
        HttpRequestMessage AccessTokenRequest(string code);
    }
}
