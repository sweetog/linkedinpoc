﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using LinkedInPoc.Interfaces;
using LinkedInPoc.Models;
using Microsoft.Extensions.Options;

namespace LinkedInPoc.Utilties
{
    public class LinkedInRouter : ILinkedInRouter
    {
        private const string UrlParamResponseType = "response_type";
        private const string ResponseTypeValue = "code"; //API Document says should always be "code" https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin/context - Ogden 4-17-2019
        private const string UrlParamClientId = "client_id";
        private const string UrlParamRedirectUri = "redirect_uri";
        private const string UrlParamState = "state";
        private const string UrlParamScope = "scope";
        private const string UrlParamGrantType = "grant_type";
        private const string ResponseGrantType = "authorization_code"; //The value of this field should always be: authorization_code
        private const string UrlParamCode = "code";
        private const string UrlParamClientSecret = "client_secret";

        //profile permissions
        /// <summary>
        /// eRequired to retrieve name and photo for the authenticated user.mployees
        /// </summary>
        private const string LiteProfile = "r_liteprofile";
        /// <summary>
        /// Required to retrieve name and photo for the authenticated user.
        /// </summary>
        private const string BasicProfile = "r_basicprofile";
        /// <summary>
        /// Required to retrieve name and photo for the authenticated user.
        /// </summary>
        private const string FullProfile = "r_fullprofile";
        /// <summary>
        /// Post, comment and like posts on behalf of an authenticated member.
        /// </summary>
        private const string MemberSocial = "w_member_social";
        /// <summary>
        /// Retrieve primary email address on the authenticated user's behalf.
        /// </summary>
        private const string EmailAddress = "r_emailaddress";
        /// <summary>
        /// 
        /// </summary>
        private const string PrimaryContract = "r_primarycontact";


        private readonly AppSettings _settings;

        public LinkedInRouter(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
        }

        /// <summary>
        /// NOT IN USE, PERMISSIONS/SCOPE REQUEST SENT FROM FRONTEND 
        /// </summary>
        /// <param name="state">A unique code that the caller users to protect against CSRF by inspecting Authorization Code Request Response</param>
        /// <returns></returns>
        public string AuthorizationCodeRequestLightProfileUrlEncoded(string state)
        {
            UrlEncoder encoder = UrlEncoder.Default;
            var scope = $"{BasicProfile} {EmailAddress}";
            return encoder.Encode(BuildAuthorizationCodeRequestUrl(state, scope));
        }

        public HttpRequestMessage AccessTokenRequest(string code)
        {
            UrlEncoder encoder = UrlEncoder.Default;
            var nvc = new List<KeyValuePair<string, string>>();
            nvc.Add(new KeyValuePair<string, string>(UrlParamGrantType, ResponseGrantType));
            nvc.Add(new KeyValuePair<string, string>(UrlParamCode, code));
            nvc.Add(new KeyValuePair<string, string>(UrlParamRedirectUri, _settings.RedirectUri));
            nvc.Add(new KeyValuePair<string, string>(UrlParamClientId, _settings.ClientId));
            nvc.Add(new KeyValuePair<string, string>(UrlParamClientSecret, _settings.ClientSecret));
            return new HttpRequestMessage(HttpMethod.Post, _settings.LinkedInAccessTokenUrl) { Content = new FormUrlEncodedContent(nvc) };
        }

        public string EmailRequestUrl()
        {
            return $"{_settings.LinkedInEmailUrl}";
        }

        public string ProfileRequestUrl()
        {
            return $"{_settings.LinkedInProfileUrl}";
        }

        #region Private Helpers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state">A unique code that the caller users to protect against CSRF by inspecting Authorization Code Request Response</param>
        /// <param name="scope">Space delimited profile permissions being requested</param>
        /// <returns></returns>
        private string BuildAuthorizationCodeRequestUrl(string state, string scope)
        {
            return
                $"{_settings.LinkedInAuthUrl}?{UrlParamResponseType}={ResponseTypeValue}&{UrlParamClientId}={_settings.ClientId}&{UrlParamRedirectUri}={_settings.RedirectUri}&{UrlParamState}={state}&{UrlParamScope}={scope}";
        }
        #endregion
    }
}
