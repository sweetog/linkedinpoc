﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace LinkedInPoc.Interfaces
{
    public interface ILinkedInService
    {
        Task<string> GetAccessToken(string code);
        Task<JObject> GetProfile(string accessToken);
        Task<JObject> GetEmail(string accessToken);
    }
}
