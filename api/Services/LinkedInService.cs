﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LinkedInPoc.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LinkedInPoc.Services
{
    public class LinkedInService: ILinkedInService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILinkedInRouter _router;

        public LinkedInService(IHttpClientFactory clientFactory, ILinkedInRouter router)
        {
            _clientFactory = clientFactory;
            _router = router;
        }

        public async Task<JObject> GetProfile(string accessToken)
        {
            var requestUrl = _router.ProfileRequestUrl();
            var resp = await PerformRequest(accessToken, requestUrl);
            return resp;
        }

        public async Task<JObject> GetEmail(string accessToken)
        {
            var requestUrl = _router.EmailRequestUrl();
            var resp = await PerformRequest(accessToken, requestUrl);
            return resp;
        }


        public async Task<string> GetAccessToken(string code)
        {
            var client = new HttpClient();
            HttpRequestMessage request = _router.AccessTokenRequest(code);

            HttpResponseMessage response = await client.SendAsync(request);

            var jsonString = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode) return null;

            JObject json = JObject.Parse(jsonString);

            return json.GetValue("access_token").ToString();

        }

        #region Private Helpers

        private async Task<JObject> PerformRequest(string accessToken, string requestUrl)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            HttpClient client = _clientFactory.CreateClient();

            HttpResponseMessage response = await client.SendAsync(request);

            var jsonString = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode) return null;

            return JObject.Parse(jsonString);
        }

        #endregion
    }
}
