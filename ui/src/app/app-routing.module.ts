import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinkedInSignIn } from './linkedin-signin.component';
import { LinkedInExample } from './linkedin-example.component';

const routes: Routes = [
  {
    path: 'signin-linkedin',
    component: LinkedInSignIn
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routableComponents = [
  LinkedInSignIn,
  LinkedInExample
];