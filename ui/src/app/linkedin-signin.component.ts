import { Component, OnInit } from '@angular/core';
import { LinkedInProfileService } from './@core/linkedin-profile.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
    template: '<h2>{{profile | json }}</h2>'
})

export class LinkedInSignIn implements OnInit {
    code: string;
    profile: any = "Processing"

    constructor(private linkedInProfileService: LinkedInProfileService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        //parse out code
        //send to api
        //api retrieves token, then gets profile
        //and returns profile

        this.route.queryParams.subscribe(params => {
            this.code = params['code'];

            console.log('code:', this.code);

            if (!this.code) { return };

            console.log('calling linkedInProfileService.getProfile');
            this.linkedInProfileService.getProfile(this.code)
                .subscribe((resp: String) => {
                    this.profile = resp
                    console.log('resp', JSON.stringify(resp));
                });
        });

        // this.route.params
        //     .subscribe((params: Params) => {
        //         this.code = params['code'];
       
        //     });
    }
}